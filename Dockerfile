FROM node:12.18.3-buster as build-stage

RUN mkdir -p /home/node/node_modules && chown -R node:node /home/node

WORKDIR /home/node

USER node

COPY --chown=node:node package*.json /home/node/

RUN npm install

COPY --chown=node:node ./ /home/node/

RUN npm run build -- --production

FROM nginx:1.15

COPY --from=build-stage /home/node/build/ /usr/share/nginx/html

COPY --from=build-stage /home/node/src/nginx.conf /etc/nginx/conf.d/default.conf
