import { FETCH_EXCHANGE_RATES_TODAY, SELECT_CURRENCY } from '../types'

const currencyReducer = (
	state = {
		currencySelected: localStorage.getItem('currencySelected') || 'eur',
		exchangeRates: JSON.parse(localStorage.getItem('exchangeRates') || '{}'),
	},
	action
) => {
	switch (action.type) {
		case SELECT_CURRENCY:
			return {
				currencySelected: action.payload.currency,
				exchangeRates: state.exchangeRates
			}
		case FETCH_EXCHANGE_RATES_TODAY:
			return {
				currencySelected: state.currencySelected,
				exchangeRates: action.payload
			}
		default:
			return state
	}
}

export default currencyReducer
