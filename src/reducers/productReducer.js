import {
	FETCH_PRODUCTS,
	GET_PRODUCTS_ERROR,
	GET_PRODUCTS_LOADING,
	GET_PRODUCTS_SUCCESS,
	FILTER_PRODUCTS_BY_SIZE,
	ORDER_PRODUCTS_BY_PRICE,
	// Search
	SEARCH_PRODUCTS,
	SEARCH_PRODUCTS_LOADING,
} from '../types'

const initialState = {
	errorHome: false,
	errorSearch: false,
	items: [],
	itemsByCurrency: {},
	loadingHome: false,
	loadingSearch: false
}

const productsReducer = (state = initialState, action) => {
	state.items = JSON.parse(localStorage.getItem('defaultProductData') || '[]')

	switch (action.type) {
		case FILTER_PRODUCTS_BY_SIZE:
			return {
				...state,
				size: action.payload.size,
				filteredItems: action.payload.items,
			}
		case ORDER_PRODUCTS_BY_PRICE:
			return {
				...state,
				sort: action.payload.sort,
				filteredItems: action.payload.items,
			}
		case FETCH_PRODUCTS:
			return { items: action.payload, filteredItems: action.payload }
		// Get Home
		case GET_PRODUCTS_LOADING:
			return {
				...state,
				loadingHome: true
			}
		case GET_PRODUCTS_ERROR:
			return {
				...initialState,
				errorHome: action.payload.error
			}
		case GET_PRODUCTS_SUCCESS:
			return {
				...state,
				items: action.payload.data,
				filteredItems: action.payload.data,
				loadingHome: false
			}
		// Search
		case SEARCH_PRODUCTS_LOADING:
			return {
				error: false,
				items: [],
				filteredItems: [],
				loading: true
			}
		case SEARCH_PRODUCTS:
			return {
				error: action.payload.error,
				items: action.payload.data,
				filteredItems: action.payload.data,
				loading: false
			}
		default:
			return state
	}
}

export default productsReducer
