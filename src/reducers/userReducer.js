import {
	// Login
	USER_LOGIN_LOADING,
	USER_LOGIN_ERROR,
	USER_LOGIN_SUCCESS,
	// Register
	USER_REGISTER_LOADING,
	USER_REGISTER_ERROR,
	USER_REGISTER_SUCCESS,
	// Current User
	GET_CURRENT_USER_ERROR,
	GET_CURRENT_USER_SUCCESS,
	USER_LOGOUT_SUCCESS,
	USER_LOGOUT_ERROR
} from '../types'

const initialState = {
	loadingLogin: false,
	loadingRegister: false,
	currentUser: null,
	errorCurrentUser: false,
	errorLogin: false,
	errorLogout: false,
	errorRegister: false
}

const userReducer = (state = initialState, action) => {
	switch (action.type) {
		// Login
		case USER_LOGIN_LOADING:
			return {
				...initialState,
				loadingLogin: true,
			}
		case USER_LOGIN_ERROR:
			return {
				...initialState,
				errorLogin: action.payload.error,
			}
		case USER_LOGIN_SUCCESS:
			return {
				...initialState,
				currentUser: action.payload.user,
			}
		// Register
		case USER_REGISTER_LOADING:
			return {
				...initialState,
				loadingRegister: true,
			}
		case USER_REGISTER_ERROR:
			return {
				...initialState,
				errorRegister: action.payload.error,
			}
		case USER_REGISTER_SUCCESS:
			return {
				...initialState,
				currentUser: action.payload.user,
			}
		// Current User
		case GET_CURRENT_USER_ERROR:
			return {
				...initialState,
				errorCurrentUser: action.payload.error,
			}
		case GET_CURRENT_USER_SUCCESS:
			return {
				...state,
				currentUser: action.payload.user,
			}
		// Logout
		case USER_LOGOUT_ERROR:
			return {
				...initialState,
				errorLogout: action.payload.error,
			}
		case USER_LOGOUT_SUCCESS:
			return {
				...initialState
			}
		default:
			return state
	}
}

export default userReducer
