import {
	CLEAR_ORDER,
	GET_ORDERS_SUCCESS,
	// Create order
	CREATE_ORDER_ERROR,
	CREATE_ORDER_LOADING,
	CREATE_ORDER_SUCCESS,
	CLEAR_ORDERS,
} from '../types'

const initialState = {
	loadingCreateOrder: false,
	order: null,
	orders: [],
	errorCreateOrder: false,
}

const orderReducer = (state = initialState, action) => {
	switch (action.type) {
		// Create order
		case CREATE_ORDER_ERROR:
			return {
				...initialState,
				errorCreateOrder: true
			}
		case CREATE_ORDER_LOADING:
			return {
				...initialState,
				loadingCreateOrder: true
			}
		case CREATE_ORDER_SUCCESS:
			return {
				...initialState,
				order: action.payload
			}
		case CLEAR_ORDER:
			return {
				...initialState,
				order: null
			}
		case GET_ORDERS_SUCCESS:
			return {
				...initialState,
				orders: action.payload
			}
		case CLEAR_ORDERS:
			return {
				...initialState
			}
		default:
			return state
	}
}
export default orderReducer
