import {
	ADD_TO_CART,
	COUNT_PRODUCT,
	REMOVE_FROM_CART,
	CLEAR_CART,
	ADD_SHIPPING_COST,
	REMOVE_SHIPPING_COST,
	CHANGE_CURRENCY_TO_ITEMS
} from '../types'

const initialState = {
	cartItems: [],
	shippingCost: 0,
}

const cartReducer = (state = initialState, action) => {
	state.cartItems = JSON.parse(localStorage.getItem('cartItems') || '[]')

	switch (action.type) {
		case ADD_TO_CART:
			return {
				...state,
				cartItems: action.payload.cartItems
			}
		case ADD_SHIPPING_COST:
			return {
				...state,
				shippingCost: action.payload.shippingCost
			}
		case REMOVE_SHIPPING_COST:
			return {
				...state,
				shippingCost: 0
			}
		case COUNT_PRODUCT:
			return {
				...state,
				cartItems: action.payload.cartItems
			}
		case CLEAR_CART:
			return initialState
		case CHANGE_CURRENCY_TO_ITEMS:
			return {
				...state,
				cartItems: action.payload.cartItems
			}
		case REMOVE_FROM_CART:
			return {
				...state,
				cartItems: action.payload.cartItems
			}
		default:
			return state
	}
}

export default cartReducer
