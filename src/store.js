import { createStore, compose, combineReducers, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'

import cartReducer from './reducers/cartReducer'
import currencyReducer from './reducers/currencyReducer'
import productsReducer from './reducers/productReducer'
import orderReducer from './reducers/orderReducer'
import userReducer from './reducers/userReducer'

const initialState = {}
const composeEnhacer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

const store = createStore(
	combineReducers({
		cart: cartReducer,
		currency: currencyReducer,
		products: productsReducer,
		order: orderReducer,
		user: userReducer,
	}),
	initialState,
	composeEnhacer(applyMiddleware(thunk))
)

export default store
