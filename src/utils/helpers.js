/**
 * Format to currency number
 * @param {number} amount
 * @param {string} symbol
 */
export const formatNumberCurrency = (amount, symbol = '€') => {
	if (!amount || typeof amount.toFixed !== 'function') return null

	if (symbol === null || symbol === '') symbol = '€'

	return `${symbol} ${amount.toFixed(2).toString().replace('.', ',').replace(/\B(?=(\d{3})+(?!\d))/g, '.')}`
}
