import React from 'react'
import { useHistory /*useRouteMatch*/ } from 'react-router-dom'
import {
	AppBar,
	Badge,
	IconButton,
	makeStyles,
	Menu,
	MenuItem,
	Toolbar,
	Tooltip,
	Typography,
	Button,
} from '@material-ui/core'
import {
	ShoppingCart as ShoppingCartIcon,
	AccountCircle,
	AttachMoney as DollarIcon,
	Euro as EuroIcon,
	Home as HomeIcon,
} from '@material-ui/icons'
import { connect } from 'react-redux'

// Internal actions
import { selectCurrency } from '../../actions/currencyActions'
import { getCurrentUserAction, logoutAction } from '../../actions/userActions'

// Internal components
import CartPreview from '../Cart/CartPreview.js'
import HeaderSearch from './HeaderSearch'

// Internal hooks
import useCartItems from '../../hooks/useCartItems'
import useCurrencySelected from '../../hooks/useCurrencySelected'
import useCurrentUser from '../../hooks/useCurrentUser'

const useStyles = makeStyles((theme) => ({
	root: {
		flexGrow: 1,
	},
	appBar: {
		// position: 'relative',
		background: theme.palette.error.main,
		minHeight: 64,
	},
	appTitle: {
		color: '#000',
		cursor: 'pointer',
		flexGrow: 1,
		[theme.breakpoints.down('xs')]: {
			display: 'none'
		},
	},
	homeButton: {
		color: theme.palette.common.white,
		display: 'none',
		[theme.breakpoints.down('xs')]: {
			display: 'block',
			marginRight: theme.spacing(1)
		},
	},
	iconButton: {
		color: theme.palette.common.white,
		[theme.breakpoints.down('xs')]: {
			margin: 0,
			padding: 0,
		},
	},
	iconUserTypography: {
		[theme.breakpoints.down('xs')]: {
			display: 'none',
		},
	},
}))

const SHOPPING_CART_ID = 'shopping-cart-preview'

/**
 * HeaderComponent
 */
const HeaderComponent = ({
	getCurrentUserAction,
	logoutAction,
	selectCurrency,
}) => {
	const classes = useStyles()
	const history = useHistory()
	// const match = useRouteMatch()
	const cartItems = useCartItems()
	const currencySelected = useCurrencySelected()
	const currentUser = useCurrentUser()

	// States
	const [isCartActive, setIsCartActive] = React.useState(false)
	const [anchorElOpenMenu, setAnchorElOpenMenu] = React.useState(null)

	// Refs
	const isMountedRef = React.useRef(true)

	// Effect isMounted component
	React.useEffect(() => {
		/**
		 * When it has been clicked outside close shopping cart preview
		 *
		 * @param {Event} event
		 */
		const handleClickOutside = (event) => {
			const cartNode = document.getElementById(SHOPPING_CART_ID)
			if (cartNode && cartNode.classList.contains('active')) {
				if (!cartNode || !cartNode.contains(event.target)) {
					setIsCartActive(false)
					event.stopPropagation()
				}
			}
		}

		if (isMountedRef.current) {
			document.addEventListener('click', handleClickOutside, true)

			// Get Current User
			// if (!currentUser) getCurrentUserAction()
		}

		return () => {
			isMountedRef.current = false
			document.removeEventListener('click', handleClickOutside, true)
		}
	}, [])

	React.useEffect(() => {
		if (!currentUser && isMountedRef.current) getCurrentUserAction()
	}, [isMountedRef, currentUser, getCurrentUserAction])

	// Event listeners
	const onClickShoppingCart = () => {
		setIsCartActive(!isCartActive)
	}

	/**
	 * Click for open menu
	 */
	const handleClickMenuAccount = (evt) => {
		if (currentUser !== null && currentUser.id) {
			setAnchorElOpenMenu(evt.currentTarget)
			return
		}
		history.push('/login')
	}

	const handleCloseMenuAccount = (url = '') => {
		setAnchorElOpenMenu(null)
		if (url && url !== '') {
			history.push(url)
		}
	}

	/**
	 * Handle logout
	 */
	const handleLogout = () => {
		setAnchorElOpenMenu(null)
		logoutAction().then(() => history.push('/login'))
	}

	const handleSearch = (evt) => {
		if (evt.target.value !== '') {
			history.push(`/search/${evt.target.value}`)
		}
	}

	return (
		<div className={classes.root}>
			<AppBar position="relative" className={classes.appBar}>
				<Toolbar>
					<Typography
						variant="h6"
						noWrap
						className={classes.appTitle}
						onClick={() => history.push('/')}
					>
						Now Pizza
					</Typography>
					<IconButton component="span" className={classes.homeButton} onClick={() => history.push('/')}>
						<HomeIcon />
					</IconButton>
					{/* Exchange selector */}
					<Tooltip title="Change currency to Euros">
						<IconButton
							aria-label="Euro selector"
							className={currencySelected === 'eur' ? classes.iconButton : ''}
							onClick={() => selectCurrency('eur')}
						>
							<EuroIcon />
						</IconButton>
					</Tooltip>
					<Tooltip title="Change currency to Dollars">
						<IconButton
							aria-label="Dollar selector"
							className={currencySelected === 'usd' ? classes.iconButton : ''}
							onClick={() => selectCurrency('usd')}
						>
							<DollarIcon />
						</IconButton>
					</Tooltip>
					{/* Input Search */}
					<HeaderSearch onKeyDown={handleSearch} />
					{/* Acciones para sesión actual */}
					<Button
						aria-controls="current-account-menu"
						aria-haspopup="true"
						className={classes.iconButton}
						// color="primary"
						onClick={handleClickMenuAccount}
						startIcon={<AccountCircle />}
					>
						{currentUser !== null && currentUser.name && (
							<Typography
								className={classes.iconUserTypography}
							>{`${currentUser.name}`}</Typography>
						)}
						{currentUser === null && (
							<Typography className={classes.iconUserTypography}>
								User account
							</Typography>
						)}
					</Button>
					<Menu
						id="current-account-menu"
						anchorEl={anchorElOpenMenu}
						keepMounted
						open={Boolean(anchorElOpenMenu)}
						onClose={handleCloseMenuAccount}
					>
						<MenuItem onClick={() => handleCloseMenuAccount('/orders')}>
							My Orders
						</MenuItem>
						<MenuItem onClick={handleLogout}>Logout</MenuItem>
					</Menu>
					{/* View Shopping Card */}
					<IconButton
						// color="primary"
						component="span"
						aria-label="Shopping cart"
						className={classes.iconButton}
						onClick={onClickShoppingCart}
					>
						<Badge
							badgeContent={cartItems
								.map((i) => i.count)
								.reduce((a, b) => a + b, 0)}
							color="primary"
						>
							<ShoppingCartIcon />
						</Badge>
					</IconButton>
				</Toolbar>
				<CartPreview active={isCartActive} id={SHOPPING_CART_ID} />
			</AppBar>
		</div>
	)
}

export default connect(null, {
	getCurrentUserAction,
	logoutAction,
	selectCurrency,
})(HeaderComponent)
