import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import Grid from '@material-ui/core/Grid'
import Fare from '../Fare'

const useStyles = makeStyles((theme) => ({
  listItem: {
    padding: theme.spacing(1, 0),
  },
  total: {
    fontWeight: 700,
  },
  title: {
    marginTop: theme.spacing(2),
  },
}))

const ReviewComponent = ({ cartItems, contactData, shippingData }) => {
	const classes = useStyles()

  return (
    <React.Fragment>
      <Typography variant="h6" gutterBottom>
        Order summary
      </Typography>
      <List disablePadding>
        {cartItems && cartItems.length && cartItems.map((item) => (
          <ListItem className={classes.listItem} key={item.id}>
            <ListItemText primary={`${item.title} x ${item.count}`} secondary={item.description} />
            <Typography variant="body2">
							<Fare amount={item.price * item.count} symbol="$" />
						</Typography>
          </ListItem>
        ))}
      </List>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Typography variant="h6" gutterBottom className={classes.title}>
            Shipping address
          </Typography>
          <Typography gutterBottom>{`${shippingData.first_name} ${shippingData.last_name}`}</Typography>
          <List disablePadding>
						<ListItem>
							<ListItemText primary="Address line 1" secondary={shippingData.address1} />
							<ListItemText primary="Address line 2" secondary={shippingData.address2} />
						</ListItem>
						<ListItem>
							<ListItemText primary="Country" secondary={shippingData.country} />
							<ListItemText primary="City" secondary={shippingData.city} />
						</ListItem>
						<ListItem>
							<ListItemText primary="State/Province/Region" secondary={shippingData.state} />
							<ListItemText primary="Zip/Postal Code" secondary={shippingData.zip} />
						</ListItem>
					</List>
        </Grid>
				<Grid item xs={12}>
          <Typography variant="h6" gutterBottom className={classes.title}>
            Contact information
          </Typography>
          <List disablePadding>
						<ListItem>
							<Grid container>
								<Grid item md={6} xs={12}>
									<ListItemText primary="Email" secondary={contactData.email} />
								</Grid>
								<Grid item md={6} xs={12}>
									<ListItemText primary="Phone number" secondary={contactData.phone} />
								</Grid>
							</Grid>
						</ListItem>
					</List>
        </Grid>
      </Grid>
    </React.Fragment>
  )
}

export default ReviewComponent
