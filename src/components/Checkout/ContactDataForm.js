import React from 'react'
import { Formik, Form, useFormikContext } from 'formik'
import * as Yup from 'yup'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'

import InputField from '../Form/InputField'

const VALIDATION_SCHEMA = Yup.object({
	email: Yup.string()
		.email('Email is not valid')
		.matches(
			/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
			'Only valid email is allowed'
		)
		.required('Email is required'),
	phone: Yup.string()
		.matches(/^\d{2,12}$/, 'Only numbers are allowed')
		.required('Phone is required'),
})

const AutoSubmitForm = () => {
	// Grab values and submitForm from context
	const { isValid, dirty, values, submitForm } = useFormikContext()

	React.useEffect(() => {
		// Submit the form imperatively as an effect as soon as form values.token are 6 digits long
		if (isValid && dirty) {
			submitForm()
		}
	}, [isValid, dirty, values, submitForm])
	return null
}

/**
 * AddressFormComponent
 *
 * @param {function} {onSubmit}
 */
const ContactDataForm = ({ initialValues, onSubmit }) => {
	if (!initialValues) {
		initialValues = {
			email: '',
			phone: '',
		}
	}
	return (
		<React.Fragment>
			<Typography variant="h6" gutterBottom>
				Contact information
			</Typography>
			<Formik
				initialValues={initialValues}
				validationSchema={VALIDATION_SCHEMA}
				onSubmit={onSubmit}
			>
				{({ handleSubmit, isValid, values }) => (
					<Form onSubmit={handleSubmit}>
						<AutoSubmitForm />
						<Grid container spacing={3}>
							<Grid item xs={12} sm={6}>
								<InputField
									controlId={`formEmail`}
									label="Email"
									name="email"
									value={values.email || ''}
									required={true}
									type="email"
									fullWidth
								/>
							</Grid>
							<Grid item xs={12} sm={6}>
								<InputField
									controlId={`formHhone`}
									label="Phone number"
									name="phone"
									value={values.phone || ''}
									required={true}
									type="tel"
									fullWidth
								/>
							</Grid>
						</Grid>
					</Form>
				)}
			</Formik>
		</React.Fragment>
	)
}

export default ContactDataForm
