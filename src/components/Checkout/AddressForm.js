import React from 'react'
import { Formik, Form, useFormikContext } from 'formik'
import * as Yup from 'yup'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'

import InputField from '../Form/InputField'

const VALIDATION_SCHEMA = Yup.object({
	first_name: Yup.string().matches(/[a-zA-Z]$/, 'Only letters are allowed').required('First name is required'),
	last_name: Yup.string().matches(/[a-zA-Z]$/, 'Only letters are allowed').required('Last name is required'),
	address1: Yup.string().required('Address line 1 is required'),
	address2: Yup.string().nullable(true),
	country: Yup.string().matches(/[a-zA-Z]$/, 'Only letters are allowed').required('Country is required'),
	state: Yup.string().required('State/Province/Region name is required'),
	city: Yup.string().required('City is required'),
	zip: Yup.string().required('Zip/Postal code is required'),
})

const AutoSubmitForm = () => {
	// Grab values and submitForm from context
	const { isValid, dirty, values, submitForm } = useFormikContext()

	React.useEffect(() => {
		// Submit the form imperatively as an effect as soon as form values.token are 6 digits long
		if (isValid && dirty) {
			submitForm()
		}
	}, [isValid, dirty, values, submitForm])
	return null
}

/**
 * AddressFormComponent
 *
 * @param {function} {onSubmit}
 */
const AddressFormComponent = ({ initialValues, onSubmit }) => {
	if (!initialValues) {
		initialValues = {
			type: 'shipping',
			first_name: '',
			last_name: '',
			address1: '',
			address2: '',
			country: '',
			state: '',
			city: '',
			zip: '',
		}
	}

	return (
		<React.Fragment>
			<Typography variant="h6" gutterBottom>
				Shipping address
			</Typography>
			<Formik
				initialValues={initialValues}
				validationSchema={VALIDATION_SCHEMA}
				enableReinitialize
				onSubmit={onSubmit}
			>
				{({ handleSubmit, isValid, values }) => (
					<Form onSubmit={handleSubmit}>
						<AutoSubmitForm />
						<Grid container spacing={3}>
							<Grid item xs={12} sm={6}>
								<InputField
									controlId={`formFirstName`}
									label="First name"
									name="first_name"
									value={values.first_name || ''}
									required={true}
									fullWidth
								/>
							</Grid>
							<Grid item xs={12} sm={6}>
								<InputField
									controlId={`formLastName`}
									label="Last name"
									name="last_name"
									value={values.last_name || ''}
									required={true}
									fullWidth
								/>
							</Grid>
							<Grid item xs={12}>
								<InputField
									controlId={`formAddress1`}
									label="Address line 1"
									name="address1"
									value={values.address1 || ''}
									required={true}
									fullWidth
								/>
							</Grid>
							<Grid item xs={12}>
								<InputField
									controlId={`formAddress2`}
									label="Address line 2"
									name="address2"
									value={values.address2 || ''}
									fullWidth
								/>
							</Grid>
							<Grid item xs={12} sm={6}>
								<InputField
									controlId={`formCountry`}
									label="Country"
									name="country"
									value={values.country || ''}
									required={true}
									fullWidth
								/>
							</Grid>
							<Grid item xs={12} sm={6}>
								<InputField
									controlId={`formState`}
									label="State/Province/Region"
									name="state"
									value={values.state || ''}
									required={true}
									fullWidth
								/>
							</Grid>
							<Grid item xs={12} sm={6}>
								<InputField
									controlId={`formCity`}
									label="City"
									name="city"
									value={values.city || ''}
									required={true}
									fullWidth
								/>
							</Grid>
							<Grid item xs={12} sm={6}>
								<InputField
									controlId={`formZip`}
									label="Zip/Postal code"
									name="zip"
									value={values.zip || ''}
									required={true}
									fullWidth
								/>
							</Grid>
						</Grid>
					</Form>
				)}
			</Formik>
		</React.Fragment>
	)
}

export default AddressFormComponent
