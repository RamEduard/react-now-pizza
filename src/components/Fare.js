/**
 * Fare component
 *
 * @param {number} {amount
 * @param {string} symbol}
 */
const Fare = ({ amount, symbol }) => {
	if (!amount || typeof amount.toFixed !== 'function') return null

	if (symbol === null || symbol === '') symbol = '€'

	return `${symbol} ${amount.toFixed(2).toString().replace('.', ',').replace(/\B(?=(\d{3})+(?!\d))/g, '.')}`
}

Fare.defaultProps = {
	amount: 0,
	symbol: '€'
}

export default Fare
