import React from 'react'
import { Link } from 'react-router-dom'
import { Breadcrumbs, Typography, Link as MaterialLink } from '@material-ui/core'

const Breadcrumb = ({ links }) => {
	return (
		<Breadcrumbs aria-label="breadcrumb">
			{links && links.length > 0 && links.map(link => {
				if (link.path) {
					return <MaterialLink key={link.path} to={link.path} color="inherit" component={Link}>{link.label}</MaterialLink>
				}
				return <Typography key={link.label} color="textPrimary">{link.label}</Typography>
			})}
		</Breadcrumbs>
	)
}

Breadcrumb.defaultProps = {
	links: [
		{
			path: '/',
			label: 'Home'
		}
	]
}

export default Breadcrumb
