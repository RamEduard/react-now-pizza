import React from "react";
import { Grid, makeStyles, Paper, Typography, Button } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
	mainFeaturedPost: {
		position: "relative",
		backgroundColor: theme.palette.grey[800],
		color: theme.palette.common.white,
		marginBottom: theme.spacing(4),
		backgroundImage: "url(https://source.unsplash.com/random)",
		backgroundSize: "cover",
		backgroundRepeat: "no-repeat",
		backgroundPosition: "center",
	},
	overlay: {
		position: "absolute",
		top: 0,
		bottom: 0,
		right: 0,
		left: 0,
		backgroundColor: "rgba(0,0,0,.4)",
	},
	mainFeaturedPostContent: {
		position: "relative",
		padding: theme.spacing(3),
		textAlign: 'center',
		[theme.breakpoints.up("md")]: {
			padding: theme.spacing(6),
			paddingBottom: theme.spacing(10),
			paddingTop: theme.spacing(8),
		},
	},
	imgLogo: {
		margin: '0 auto',
	}
}));

const Banner = ({ data, noButton, noLogo }) => {
	const classes = useStyles();

	if (!data) return null;

	return (
		<Paper
			className={classes.mainFeaturedPost}
			style={{ backgroundImage: `url(${data.bgImageSrc})` }}
		>
			{/* Increase the priority of the hero background image */}
			{
				<img
					style={{ display: "none" }}
					src={data.bgImageSrc}
					alt={data.imageText}
				/>
			}
			<div className={classes.overlay} />
			<Grid container justify="center">
				<Grid item md={6}>
					<div className={classes.mainFeaturedPostContent}>
						{!noLogo && <img className={classes.imgLogo} src={data.logoImageSrc} alt="Logo" />}
						{data && data.title && (
							<Typography variant="h4" color="inherit" paragraph>
								{data.title}
							</Typography>
						)}
						{data && data.description && (
							<Typography variant="h5" color="inherit" paragraph>
								{data.description}
							</Typography>
						)}
						{!noButton && <Button color="secondary" variant="contained">Order now</Button>}
					</div>
				</Grid>
			</Grid>
		</Paper>
	);
};

export default Banner;
