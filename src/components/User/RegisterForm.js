import React from 'react'
import { Formik, Form } from 'formik'
import * as Yup from 'yup'
import Button from '@material-ui/core/Button'
import { makeStyles } from '@material-ui/core/styles'

// Internal components
import InputField from '../Form/InputField'

const VALIDATION_SCHEMA = Yup.object({
	email: Yup.string()
		.email('Email is not valid')
		.matches(
			/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
			'Only valid email is allowed'
		)
		.required('Email is required'),
	name: Yup.string().matches(/[a-zA-Z]$/, 'Only letters are allowed').required('Full name is required'),
	password: Yup.string().required('Password is required'),
	confirmPassword: Yup
		.string()
		.required('Confirm password is required')
		.label('Confirm password')
		.test('passwords-match', 'Passwords must match', function(value) {
			return this.parent.password === value;
		}),
})

const useStyles = makeStyles((theme) => ({
	avatar: {
		margin: theme.spacing(1),
		backgroundColor: theme.palette.secondary.main,
	},
	form: {
		width: '100%', // Fix IE 11 issue.
		marginTop: theme.spacing(1),
	},
	submit: {
		margin: theme.spacing(3, 0, 2),
	},
}))

const RegisterForm = ({ initialValues, loading, onSubmit }) => {
	const classes = useStyles()

	if (!initialValues) {
		initialValues = {
			email: '',
			name: '',
			password: '',
		}
	}

	return (
		<Formik
			initialValues={initialValues}
			validationSchema={VALIDATION_SCHEMA}
			onSubmit={onSubmit}
		>
			{({ handleSubmit, values }) => (
				<Form className={classes.form} onSubmit={handleSubmit}>
					<InputField
						controlId={`formName`}
						label="Full name"
						name="name"
						value={values.name || ''}
						required={true}
						type="name"
						fullWidth
					/>
					<InputField
						controlId={`formEmail`}
						label="Email"
						name="email"
						value={values.email || ''}
						required={true}
						type="email"
						fullWidth
					/>
					<InputField
						controlId={`formPassword`}
						label="Password"
						name="password"
						value={values.password || ''}
						required={true}
						type="password"
						fullWidth
					/>
					<InputField
						controlId={`formConfirmPassword`}
						label="Confirm password"
						name="confirmPassword"
						value={values.confirmPassword || ''}
						required={true}
						type="password"
						fullWidth
					/>
					<Button
						fullWidth
						variant="contained"
						color="primary"
						className={classes.submit}
						onClick={handleSubmit}
						disabled={loading}
					>
						Register
					</Button>
				</Form>
			)}
		</Formik>
	)
}

export default RegisterForm
