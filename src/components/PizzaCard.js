import React from 'react'
import {
	Button,
	Card,
	CardActions,

	CardContent,
	CardMedia,

	Grid,

	InputBase,
	MenuItem,
	Typography,
	makeStyles,
	colors,
	Select,
	FormControl,
} from '@material-ui/core'
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart'

// Internal components
import InputAddSubstract from './Form/InputAddSubstract'

// Helpers
import { formatNumberCurrency } from '../utils/helpers'

const useStyles = makeStyles((theme) => ({
	card: {
		height: '100%',
		display: 'flex',
		flexDirection: 'column',
	},
	cardMedia: {
		paddingTop: '56.25%', // 16:9
	},
	cardContent: {
		flexGrow: 1,
	},
	chipMargin: {
		color: '#ffffff',
		marginRight: theme.spacing(1),
	},
	chipGreen: {
		background: colors.green.A400,
	},
	chipBlue: {
		background: colors.blue.A200,
	},
	addToCartButton: {
		background: colors.common.white,
	},
	inputPadding: {
		'& > div': {
			padding: '10px 15px',
    },
    borderRadius: 0
  },
  inputAmount: {
    border: '1px solid rgba(0, 0, 0, 0.2)',
    borderRadius: 0,
    padding: '2.5px 0',
    '&:hover': {
      border: '1px solid rgba(0, 0, 0, 0.87)',
    }
  }
}))

const PizzaCard = ({
	currency,
	description,
	image,
	images,
	price: mainPrice,
	sizes,
	title,
	onHandleOrder,
}) => {
	const classes = useStyles()

	const [count, setCount] = React.useState(1)
	const [price, setPrice] = React.useState()
	const [size, setSize] = React.useState()

	const setProductSize = (size, price) => {
		setSize(size)
		setPrice(price)

		console.log(size, price);
	}

	React.useEffect(() => {
		if (!sizes && mainPrice) {
			return setPrice(Number(mainPrice))
		}

		if (sizes && sizes.length > 0) {
			setSize((sizes[0]).size)
			setPrice(Number((sizes[0]).price))
		}

	}, [mainPrice, sizes])

	return (
		<Grid item xs={12} sm={6} md={4} lg={3}>
			<Card className={classes.card}>
				<CardMedia className={classes.cardMedia} image={image} title={title} />
				<CardContent className={classes.cardContent}>
					<Typography gutterBottom variant="h5" component="h2">
						{title}
					</Typography>
					<Typography>{description}</Typography>
					<Typography gutterBottom component="h6" variant="h6"></Typography>
				</CardContent>
				<Grid container>
					<Grid item xs={4}>
						<FormControl variant="outlined" fullWidth>
							{/* <InputLabel id="demo-simple-select-filled-label">
								Size:{" "}
							</InputLabel> */}
							<Select
								labelId="demo-simple-select-filled-label"
								id="demo-simple-select-filled"
								value={!!size ? size : ''}
								onChange={e => setProductSize(e.target.value, (sizes.find(s => e.target.value === s.size)).price)}
								className={classes.inputPadding}
							>
								{sizes && sizes.length > 0 && sizes.map(d => (
									<MenuItem value={d.size} key={d.id}>{`${d.size.charAt(0).toUpperCase() + d.size.slice(1)}`}</MenuItem>
								))}
							</Select>
						</FormControl>
					</Grid>
					<Grid item xs={4}>
						<FormControl variant="outlined" fullWidth>
							<InputAddSubstract
								defaultValue={count}
								onChange={e => {
									if (!/\d$/.test(e.target.value)) {
										e.target.value = 1
										return
									} else if (e.target.value > 0) {
										setCount(e.target.value)
									}
								}}
								onBlur={e => {
									if (!/\d$/.test(e.target.value)) {
										e.target.value = 1
										return
									} else if (e.target.value === 0) {
										e.target.value = 1
									}
								}}
							/>
						</FormControl>
					</Grid>
					<Grid item xs={4}>
						<FormControl variant="outlined" fullWidth>
              <InputBase
								className={classes.inputAmount}
								value={!!price ? formatNumberCurrency(price * count, currency === 'usd' ? '$' : '€') : ''}
								readOnly
              />
						</FormControl>
					</Grid>
				</Grid>
				<CardActions>
					<Button
						size="small"
						color="secondary"
						onClick={() => onHandleOrder({ count, price, size })}
						fullWidth
						startIcon={<AddShoppingCartIcon />}
						variant="contained"
					>
						Add to Cart
					</Button>
				</CardActions>
			</Card>
		</Grid>
	)
}

PizzaCard.defaultProps = {
	description:
		'This is a media card. You can use this section to describe the content.',
	image: 'https://source.unsplash.com/random',
	price: '$ 12.34',
	title: 'Pizza Title',
}

export default PizzaCard
