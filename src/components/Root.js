import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'

import Routes from '../routes'
import store from '../store'

const Root = () => (
	<Provider store={store}>
		<Routes />
	</Provider>
)

export default Root

if (document.getElementById('root')) {
	ReactDOM.render(<Root />, document.getElementById('root'))
}
