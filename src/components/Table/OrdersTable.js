import React from 'react'
import { Link } from 'react-router-dom'
import {
	makeStyles,
	Table,
	TableBody,
	TableCell,
	TableContainer,
	TableHead,
	TableRow,
} from '@material-ui/core'

// Internal components
import Fare from '../Fare'
import moment from 'moment'

const useStyles = makeStyles({
	table: {
		minWidth: 650,
	},
})

const OrdersTableComponent = ({ data }) => {
	const classes = useStyles()

console.log(data)

	return (
		<TableContainer>
			<Table className={classes.table} aria-label="simple table">
				<TableHead>
					<TableRow>
						<TableCell>Order ID</TableCell>
						<TableCell>Date</TableCell>
						<TableCell align="right">Subtotal</TableCell>
						<TableCell align="right">Shipping</TableCell>
						<TableCell align="right">Total</TableCell>
						<TableCell align="right">Status</TableCell>
					</TableRow>
				</TableHead>
				<TableBody>
					<React.Fragment>
						{!data ||
							(data.length <= 0 && (
								<TableRow>
									<TableCell scope="row" colSpan={7} align="center">
										There are not orders yet.
									</TableCell>
								</TableRow>
							))}
						{data &&
							data.length > 0 &&
							data.map(order => (
								<TableRow key={order.id}>
									<TableCell component="th" scope="row">
										<Link to={`/order/${order.id}`}>#{order.id}</Link>
									</TableCell>
									<TableCell>{`${moment(order.created_at).format(
										'DD MMM YYYY HH:mm'
									)}`}</TableCell>
									<TableCell align="right">
										<Fare amount={order.subtotal} symbol={order.currency === 'usd' ? '$' : '€'} />
									</TableCell>
									<TableCell align="right">
										{order.shipping === 0 ? (
											'Free shipping'
											) : (
												<Fare amount={order.shipping} symbol={order.currency === 'usd' ? '$' : '€'} />
												)}
									</TableCell>
									<TableCell align="right">
										<Fare amount={order.total} symbol={order.currency === 'usd' ? '$' : '€'} />
									</TableCell>
									<TableCell align="right">{order.status}</TableCell>
								</TableRow>
							))}
					</React.Fragment>
				</TableBody>
			</Table>
		</TableContainer>
	)
}

export default OrdersTableComponent
