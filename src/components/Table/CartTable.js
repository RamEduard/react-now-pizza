import React from 'react'
import {
	Avatar,
	IconButton,
	Paper,
	Table,
	TableBody,
	TableCell,
	TableContainer,
	TableHead,
	TableRow,
	colors,
	withStyles,
	FormControl,
} from '@material-ui/core'
import { Delete as DeleteIcon } from '@material-ui/icons'

// Internal components
import Fare from '../Fare'
import InputAddSubstract from '../Form/InputAddSubstract'

const StyledTableCell = withStyles((theme) => ({
	head: {
		backgroundColor: colors.orange.A700,
		color: theme.palette.common.white,
	},
	body: {
		fontSize: 14,
	},
}))(TableCell)

const StyledTableRow = withStyles((theme) => ({
	root: {
		'&:nth-of-type(odd)': {
			backgroundColor: theme.palette.action.hover,
		},
	},
}))(TableRow)

const CustomProductImage = withStyles((theme) => ({
	root: {
		width: theme.spacing(12),
		height: theme.spacing(12),
		borderRadius: 0,
	},
}))(Avatar)

const CartTable = ({ currency, items, onCountProduct, onRemoveFromCart }) => {
	return (
		<TableContainer component={Paper}>
			<Table aria-label="customized table">
				<TableHead>
					<TableRow>
						<StyledTableCell></StyledTableCell>
						<StyledTableCell></StyledTableCell>
						<StyledTableCell>Item</StyledTableCell>
						<StyledTableCell align="right">Quantity</StyledTableCell>
						<StyledTableCell align="right">Price</StyledTableCell>
						<StyledTableCell align="right">Subtotal</StyledTableCell>
					</TableRow>
				</TableHead>
				<TableBody>
					{(!items || items.length <= 0) && (
						<StyledTableRow>
							<StyledTableCell scope="row" colSpan={6} align="center">
								Your cart is empty.
							</StyledTableCell>
						</StyledTableRow>
					)}
					{items &&
						items.length > 0 &&
						items.map((item) => (
							<StyledTableRow key={item.id}>
								<StyledTableCell scope="row">
									<IconButton
										edge="end"
										aria-label="remove"
										color="secondary"
										size="small"
										onClick={() => onRemoveFromCart(item)}
									>
										<DeleteIcon />
									</IconButton>
								</StyledTableCell>
								<StyledTableCell scope="row">
									<CustomProductImage src={item.image} />
								</StyledTableCell>
								<StyledTableCell scope="row">
									{`${item.title} - ${item.size.charAt(0).toUpperCase() + item.size.slice(1)}`}
								</StyledTableCell>
								<StyledTableCell align="right" style={{ maxWidth: 50 }}>
									<FormControl>
										<InputAddSubstract
											defaultValue={item.count}
											onChange={(e) => {
												if (e.target.value) {
													item.count = e.target.value
													onCountProduct(item)
												}
											}}
											onBlur={(e) => console.log('Blur', e.target.value)}
										/>
									</FormControl>
								</StyledTableCell>
								<StyledTableCell align="right">
									<Fare amount={item.price} symbol={item.currency === 'usd' ? '$' : '€'} />
								</StyledTableCell>
								<StyledTableCell align="right">
									<Fare amount={item.count * item.price} symbol={item.currency === 'usd' ? '$' : '€'} />
								</StyledTableCell>
							</StyledTableRow>
						))}
				</TableBody>
			</Table>
		</TableContainer>
	)
}

export default CartTable
