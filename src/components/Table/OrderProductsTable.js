import React from 'react'
import {
	Avatar,
	Paper,
	Table,
	TableBody,
	TableCell,
	TableContainer,
	TableHead,
	TableRow,
	colors,
	withStyles,
	Typography,
} from '@material-ui/core'
import Fare from '../Fare'

const StyledTableCell = withStyles(theme => ({
	head: {
		backgroundColor: colors.orange.A700,
		color: theme.palette.common.white,
	},
	body: {
		fontSize: 14,
	},
}))(TableCell)

const StyledTableRow = withStyles(theme => ({
	root: {
		'&:nth-of-type(odd)': {
			backgroundColor: theme.palette.action.hover,
		},
	},
}))(TableRow)

const CustomProductImage = withStyles(theme => ({
	root: {
		width: theme.spacing(12),
		height: theme.spacing(12),
		borderRadius: 0,
	},
}))(Avatar)

const OrderProductsTable = ({ currency, products }) => {
	return (
		<TableContainer component={Paper}>
			<Table aria-label="customized table">
				<TableHead>
					<TableRow>
						<StyledTableCell></StyledTableCell>
						<StyledTableCell>Item</StyledTableCell>
						<StyledTableCell align="right">Quantity</StyledTableCell>
						<StyledTableCell align="right">Price</StyledTableCell>
						<StyledTableCell align="right">Subtotal</StyledTableCell>
					</TableRow>
				</TableHead>
				<TableBody>
					{(!products || products.length <= 0) && (
						<StyledTableRow>
							<StyledTableCell scope="row" colSpan={6} align="center">
								Order has not products added.
							</StyledTableCell>
						</StyledTableRow>
					)}
					{products &&
						products.length > 0 &&
						products.map(item => (
							<StyledTableRow key={item.id}>
								<StyledTableCell scope="row">
									<CustomProductImage src={item.image} />
								</StyledTableCell>
								<StyledTableCell scope="row">
									<Typography>{`${item.title} - ${item.pivot.size.charAt(0).toUpperCase() + item.pivot.size.slice(1)}`}</Typography>
									<Typography>{`${item.description}`}</Typography>
								</StyledTableCell>
								<StyledTableCell align="right">
									{`${item.pivot.count}`}
								</StyledTableCell>
								<StyledTableCell align="right">
									<Fare amount={Number(item.pivot.price)} symbol={currency === 'usd' ? '$' : '€'} />
								</StyledTableCell>
								<StyledTableCell align="right">
									<Fare amount={item.pivot.count * item.pivot.price} symbol={currency === 'usd' ? '$' : '€'} />
								</StyledTableCell>
							</StyledTableRow>
						))}
				</TableBody>
			</Table>
		</TableContainer>
	)
}

export default OrderProductsTable
