import React from 'react'
import { makeStyles } from '@material-ui/core'

import Header from './Header/Header'

const useStyles = makeStyles((theme) => ({
	layout: {
    width: 'auto',
	},
	layoutWithMargin: {
		marginLeft: theme.spacing(2),
		marginRight: theme.spacing(2)
	}
}))

const LayoutComponent = ({ children }) => {
	const classes = useStyles()

	return (
		<React.Fragment>
			<Header />
			<main className={classes.layout}>
				{ children }
			</main>
		</React.Fragment>
	)
}

export default LayoutComponent
