import React from 'react'
import {
	IconButton,
	makeStyles,
	Paper,
	Typography,
	Button,
	ListItem,
	ListItemSecondaryAction,
	List,
	ListItemAvatar,
	Avatar,
} from '@material-ui/core'
import classNames from 'classnames'
import { Delete as DeleteIcon } from '@material-ui/icons'
import { connect } from 'react-redux'
import { useHistory } from 'react-router-dom'

// Internal components
import Fare from '../Fare'

// Actions
import { removeFromCart } from '../../actions/cartActions'
import useCartItems from '../../hooks/useCartItems'
import useCartTotals from '../../hooks/useCartTotals'

const PreviewCartItem = ({
	currency,
	image,
	price,
	count,
	size,
	title,
	removeFromCart,
}) => (
	<ListItem>
		<ListItemAvatar>
			<Avatar src={image} />
		</ListItemAvatar>
		<div>
			<Typography variant="body1">{title}</Typography>
			<Typography variant="body2">
				<Fare amount={price} symbol={currency === 'usd' ? '$' : '€'} />{' '}
				x {count} {`${size.charAt(0).toUpperCase() + size.slice(1)}`}
			</Typography>
		</div>
		<ListItemSecondaryAction>
			<IconButton
				edge="end"
				aria-label="remove"
				size="small"
				onClick={removeFromCart}
			>
				<DeleteIcon />
			</IconButton>
		</ListItemSecondaryAction>
	</ListItem>
)

PreviewCartItem.defaultProps = {
	count: 1,
}

const useStyles = makeStyles((theme) => ({
	root: {
		position: 'absolute',
		paddingTop: theme.spacing(2),
		minWidth: 300,
		minHeight: 100,
		right: theme.spacing(2),
		top: theme.spacing(8),
		[theme.breakpoints.down('xs')]: {
			right: theme.spacing(1),
			left: theme.spacing(1),
			height: '90%',
			width: `calc(100% - ${theme.spacing(2)}px)`,
			zIndex: 2,
		},
	},
	heightWithItems: {
		minHeight: 300,
	},
	showCart: {
		display: 'block',
	},
	hideCart: {
		display: 'none',
	},
	title: {
		margin: theme.spacing(1),
	},
	subtotalTitle: {
		padding: `0 ${theme.spacing(2)}px`
	},
	actionButtons: {
		position: 'absolute',
		bottom: theme.spacing(1),
		padding: theme.spacing(1),
	},
	buttonViewCart: {},
	buttonCheckout: {},
	listContainer: {
		background: theme.palette.background.paper,
	},
}))

/**
 * ShoppingCartPreview component
 *
 * @param {boolean} {active
 * @param {string}   className
 * @param {string}   id}
 */
const CartPreview = ({ active, className, id, removeFromCart }) => {
	const classes = useStyles()
	const history = useHistory()
	const cartItems = useCartItems()
	const cartTotals = useCartTotals()

	const handleRemoveFromCart = (item) => {
		removeFromCart(item)
	}

	return (
		<Paper
			className={classNames(
				classes.root,
				className,
				active === true ? classes.showCart : classes.hideCart,
				active === true ? 'active' : 'no-active',
				cartItems && cartItems.length > 0 && classes.heightWithItems
			)}
			id={id}
		>
			{(!cartItems || !cartItems.length) && (
				<Typography
					className={classes.title}
					align="center"
					component="h3"
					variant="h5"
					color="textSecondary"
				>
					Your cart is empty!
				</Typography>
			)}

			{cartItems && cartItems.length > 0 && (
				<div className={classes.listContainer}>
					<Typography className={classes.subtotalTitle} variant="body1">
						<strong>Subtotal:</strong>{' '}
						<Fare
							amount={cartTotals.total}
							symbol={cartItems[0].currency === 'usd' ? '$' : '€'}
						/>
					</Typography>
					<List dense style={{ marginBottom: 50 }}>
						{cartItems.map((item) => (
							<PreviewCartItem
								key={`${item.id}-${item.size}`}
								{...item}
								removeFromCart={(e) => handleRemoveFromCart(item)}
							/>
						))}
					</List>
				</div>
			)}

			<div className={classes.actionButtons}>
				<Button
					className={classes.buttonViewCart}
					color="secondary"
					disabled={!cartItems || !cartItems.length}
					onClick={() => history.push('/cart')}
				>
					View cart
				</Button>
				<Button
					className={classes.buttonCheckout}
					color="primary"
					disabled={!cartItems || !cartItems.length}
					onClick={() => history.push('/checkout')}
				>
					Proceed to checkout
				</Button>
			</div>
		</Paper>
	)
}

CartPreview.defaultProps = {
	active: false,
}

export default connect(null, {
	removeFromCart,
})(CartPreview)
