import React from 'react'
import {
	withStyles,
	Paper,
	Table,
	TableBody,
	TableCell,
	TableContainer,
	TableHead,
	TableRow,
	colors,
	makeStyles
} from '@material-ui/core'
import Fare from '../Fare'

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: colors.orange.A700,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell)

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow)

const useStyles = makeStyles(({
	titleBold: {
		fontWeight: 'bold'
	}
}))

const CartTotals = ({ currency, shipping, subtotal, total }) => {
	const classes = useStyles()

	return (
		<TableContainer component={Paper}>
      <Table aria-label="customized table">
        <TableHead>
          <TableRow>
            <StyledTableCell align="center" colSpan={2}>Totals</StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
					<StyledTableRow key="subtotal">
						<StyledTableCell className={classes.titleBold} component="th">Subtotal</StyledTableCell>
						<StyledTableCell align="right">
							<Fare amount={subtotal} symbol={currency === 'usd' ? '$' : '€'} />
						</StyledTableCell>
					</StyledTableRow>
					<StyledTableRow key="shipping">
						<StyledTableCell className={classes.titleBold} component="th">Shipping</StyledTableCell>
						<StyledTableCell align="right">
							{shipping === null ? (
								'Shipping fee not determined yet'
							) : shipping === 0 ? (
								'Free shipping'
							) : (
								<Fare amount={shipping} symbol={currency === 'usd' ? '$' : '€'} />
							)}
						</StyledTableCell>
					</StyledTableRow>
					<StyledTableRow key="total">
						<StyledTableCell className={classes.titleBold} component="th">Total</StyledTableCell>
						<StyledTableCell align="right">
							<Fare amount={total} symbol={currency === 'usd' ? '$' : '€'} />
						</StyledTableCell>
					</StyledTableRow>
        </TableBody>
      </Table>
    </TableContainer>
	)
}

export default CartTotals
