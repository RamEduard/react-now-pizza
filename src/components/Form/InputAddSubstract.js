import React from 'react'
import { makeStyles, InputBase, InputAdornment, IconButton } from '@material-ui/core'
import AddIcon from '@material-ui/icons/Add'
import RemoveIcon from '@material-ui/icons/Remove'

const useStyles = makeStyles(({
	root: {
    border: '1px solid rgba(0, 0, 0, 0.2)',
    borderRadius: 0,
    padding: '2.5px 0',
    '&:hover': {
      border: '1px solid rgba(0, 0, 0, 0.87)',
    }
  }
}))

const InputAddSubstract = props => {
	const classes = useStyles()

	const inputRef = React.useRef()

	const handleAdd = () => {
		if (inputRef.current.value >= 0) {
			inputRef.current.value = Number(inputRef.current.value) + 1
			props.onChange({ target: { value: inputRef.current.value }})
		}
	}

	const handleSubstract = () => {
		if (inputRef.current.value > 1) {
			inputRef.current.value = Number(inputRef.current.value) - 1
			props.onChange({ target: { value: inputRef.current.value }})
		}
	}

	return (
		<InputBase
			className={classes.root}
			startAdornment={
				<InputAdornment position="start" style={{marginRight: 5}}>
					<IconButton size="small" onClick={handleSubstract}>
						<RemoveIcon />
					</IconButton>
				</InputAdornment>
			}
			endAdornment={
				<InputAdornment position="end" style={{marginLeft: 5}}>
					<IconButton size="small" onClick={handleAdd}>
						<AddIcon />
					</IconButton>
				</InputAdornment>
			}
			inputRef={inputRef}
			{...props}
		/>
	)
}

export default InputAddSubstract
