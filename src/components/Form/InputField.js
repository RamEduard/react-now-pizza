import React from 'react'
import { Field } from 'formik'
import {
	FormControl,
	FormHelperText,
	Input,
	InputLabel
} from '@material-ui/core'

/**
 * Input Field (email|number|tel|text)
 *
 * @param {string}  {controlId
 * @param {boolean} disabled
 * @param {boolean} fullWidth
 * @param {string}  label
 * @param {string}  name
 * @param {string}  placeholder
 * @param {boolean} required
 * @param {string}  type
 * @param {string}  value}
 */
const InputField = ({ controlId, disabled, fullWidth, label, name, placeholder, required, type, value }) => (
	<Field name={name}>
		{({ field, form: { errors } }) => (
			<FormControl error={errors && !!errors[name]} fullWidth={fullWidth}>
				<InputLabel htmlFor={controlId}>{`${label}`}</InputLabel>
				<Input
					{...field}
					id={controlId}
					placeholder={placeholder || ''}
					type={type}
					disabled={disabled}
					required={required}
					value={value}
				/>
				{errors && !!errors[name] && (
					<FormHelperText id={`${controlId}-error-text`}>{`${errors[name]}`}</FormHelperText>
				)}
			</FormControl>
		)}
	</Field>
)

export default InputField
