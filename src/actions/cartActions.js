import {
	ADD_TO_CART,
	COUNT_PRODUCT,
	REMOVE_FROM_CART,
	ADD_SHIPPING_COST,
	REMOVE_SHIPPING_COST,
	CHANGE_CURRENCY_TO_ITEMS,
} from '../types'

export const addToCart = (product) => (dispatch, getState) => {
	const cartItems = getState().cart.cartItems.slice()
	if (product.count) product.count = Number(product.count)
	let alreadyExists = false
	cartItems.forEach((p) => {
		if (p.id === product.id && product.size === p.size) {
			alreadyExists = true
			p.count += product.count > 0 ? product.count : 1
		}
	})
	if (!alreadyExists) {
		cartItems.push({ ...product, count: product.count > 0 ? product.count : 1 })
	}
	dispatch({
		type: ADD_TO_CART,
		payload: { cartItems },
	})
	localStorage.setItem('cartItems', JSON.stringify(cartItems))
}

/**
 * Redux addShippingCostAction
 *
 * @param {number} shippingCost
 */
export const addShippingCostAction = (shippingCost) => (dispatch) => {
	dispatch({
		type: ADD_SHIPPING_COST,
		payload: { shippingCost: Number(shippingCost) },
	})
}

/**
 * Redus addShippingCostAction
 */
export const removeShippingCostAction = () => (dispatch) => {
	dispatch({ type: REMOVE_SHIPPING_COST })
}

/**
 * Redux changeCurrencyAction
 *
 * @param {string} currency
 */
export const changeCurrencyToItemsAction = (currency = null) => async (dispatch, getState) => {
	// Fetch products
	const cartItems = getState().cart.cartItems
	const { data: products } = getState().products.filteredItems

	if (!cartItems || !products) return

	const useProductPrice = (_currency, id) => {
		const product = products.find(p => p.attributes.currency === _currency &&  p.id === id)

		return product && product.attributes && product.attributes.price ? product.attributes.price : null
	}

	const newCartItems = cartItems.map(item => {
		item.currency = currency
		item.price = useProductPrice(currency, item.id)

		return item
	})

	dispatch({
		type: CHANGE_CURRENCY_TO_ITEMS,
		payload: { cartItems: newCartItems }
	})
	localStorage.setItem('cartItems', JSON.stringify(newCartItems))
}

export const countProduct = (product) => (dispatch, getState) => {
	const cartItems = getState().cart.cartItems.slice()

	product.count = Number(product.count)

	// Remove product
	if (product.count === 0) {
		dispatch({
			type: COUNT_PRODUCT,
			payload: {
				cartItems: cartItems.filter(
					(p) => p.id !== product.id && product.size === p.size
				),
			},
		})
		localStorage.setItem(
			'cartItems',
			JSON.stringify(
				cartItems.filter((p) => p.id !== product.id && product.size === p.size)
			)
		)
	} else if (product.count > 0) {
		cartItems.forEach((p) => {
			if (p.id === product.id && product.size === p.size) {
				p.count = product.count
			}
		})
		dispatch({
			type: COUNT_PRODUCT,
			payload: { cartItems },
		})
		localStorage.setItem('cartItems', JSON.stringify(cartItems))
	}
}

export const removeFromCart = (product) => (dispatch, getState) => {
	console.log(product)
	const cartItems = getState()
		.cart.cartItems.slice()
		.filter((p) => !(p.id === product.id && product.size === p.size))
	dispatch({ type: REMOVE_FROM_CART, payload: { cartItems } })
	localStorage.setItem('cartItems', JSON.stringify(cartItems))
}
