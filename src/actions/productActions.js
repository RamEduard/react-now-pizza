import {
	FETCH_PRODUCTS,
	SEARCH_PRODUCTS,
	FILTER_PRODUCTS_BY_SIZE,
	ORDER_PRODUCTS_BY_PRICE,
	SEARCH_PRODUCTS_LOADING,
	GET_PRODUCTS_SUCCESS,
	GET_PRODUCTS_ERROR,
	GET_PRODUCTS_LOADING,
} from '../types'
import config from '../config'

export const fetchProducts = (currency = null) => async (dispatch) => {
	const query =
		'string' === typeof currency && currency !== ''
			? `?currency=${currency}`
			: ''
	const res = await fetch(`${config.apiUrl}/products${query}`)
	const data = await res.json()
	console.log('Fetch product >>>', data)
	dispatch({
		type: FETCH_PRODUCTS,
		payload: data,
	})

	localStorage.setItem(`${currency === null ? 'default' : currency}ProductsData`, JSON.stringify(data && data.data))
}

/**
 * Redux getProductsAction
 *
 * @param {string} currency
 */
export const getProductsAction = (currency = null) => async dispatch => {
	const query =
		'string' === typeof currency && currency !== ''
			? `?currency=${currency}`
			: ''

	// Loading products
	dispatch({ type: GET_PRODUCTS_LOADING })

	try {
		const res = await fetch(`${config.apiUrl}/products${query}`)
		const data = await res.json()

		// Loaded products
		dispatch({
			type: GET_PRODUCTS_SUCCESS,
			payload: { data },
		})
		localStorage.setItem(`${currency === null ? 'default' : currency}ProductsData`, JSON.stringify(data && data.data))
	} catch (e) {
		const error = `Something wrong has occurred.`
		// Loaded products
		dispatch({
			type: GET_PRODUCTS_ERROR,
			payload: { error },
		})
	}
}

export const searchProducts = (q = '', currency = null) => async (dispatch) => {
	let query =
		'string' === typeof currency && currency !== ''
			? `?currency=${currency}`
			: ''

	if ('string' === typeof currency && currency !== '') {
		query += `&q=${q}`
	} else {
		query += `?q=${q}`
	}

	// Loading products
	dispatch({ type: SEARCH_PRODUCTS_LOADING })

	try {
		const res = await fetch(`${config.apiUrl}/products/search${query}`)
		const data = await res.json()
		console.log('Search products >>>', data)
		// Loaded products
		dispatch({
			type: SEARCH_PRODUCTS,
			payload: { data },
		})
	} catch (e) {
		const error = `Something wrong has occurred.`
		// Loaded products
		dispatch({
			type: SEARCH_PRODUCTS,
			payload: { error },
		})
	}
}

export const filterProducts = (products, size) => (dispatch) => {
	dispatch({
		type: FILTER_PRODUCTS_BY_SIZE,
		payload: {
			size: size,
			items:
				size === ''
					? products
					: products.filter((x) => x.availableSizes.indexOf(size) >= 0),
		},
	})
}
export const sortProducts = (filteredProducts, sort) => (dispatch) => {
	const sortedProducts = filteredProducts.slice()
	if (sort === 'latest') {
		sortedProducts.sort((a, b) => (a._id > b._id ? 1 : -1))
	} else {
		sortedProducts.sort((a, b) =>
			sort === 'lowest'
				? a.price > b.price
					? 1
					: -1
				: a.price > b.price
				? -1
				: 1
		)
	}
	console.log(sortedProducts)
	dispatch({
		type: ORDER_PRODUCTS_BY_PRICE,
		payload: {
			sort: sort,
			items: sortedProducts,
		},
	})
}
