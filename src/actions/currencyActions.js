import { FETCH_EXCHANGE_RATES_TODAY, SELECT_CURRENCY } from '../types'

// actions
import { changeCurrencyToItemsAction } from './cartActions'
import { getProductsAction } from './productActions'

import config from '../config'

/**
 * Redux selectCurrency
 *
 * @param {string} currency
 */
export const selectCurrency = currency => async (dispatch, getState) => {
	const currencySelected = getState().currency.currencySelected

	if (currencySelected === currency) return

	dispatch({
		type: SELECT_CURRENCY,
		payload: { currency },
	})
	localStorage.setItem('currencySelected', currency)

	// Call to getProductsAction
	await dispatch(getProductsAction(currency))

	// Call to changeCurrencyToItemsAction
	await dispatch(changeCurrencyToItemsAction(currency))
}

export const fetchExchangeRates = () => async (dispatch, getState) => {
	// Affect products
	const res = await fetch(`${config.apiUrl}/history-exchange-rates/today`)
	const data = await res.json()
	console.log('Fetch exchangeRates >>>', data)
	dispatch({
		type: FETCH_EXCHANGE_RATES_TODAY,
		payload: data,
	})
	localStorage.setItem('exchangeRates', JSON.stringify(data))
}
