import axios from 'axios'
import Cookies from 'js-cookie'

import { CLEAR_CART, CLEAR_ORDER, GET_ORDERS_ERROR, GET_ORDERS_SUCCESS, GET_ORDERS_LOADING, CREATE_ORDER_LOADING, CREATE_ORDER_SUCCESS, CREATE_ORDER_ERROR } from '../types'
import config from '../config'

/**
 * Redux createOrder
 *
 * @param {Order} order
 */
export const createOrder = order => async dispatch => {
	axios.defaults.withCredentials = true

	try {
		dispatch({
			type: CREATE_ORDER_LOADING,
			payload: { loadingCreateOrder: true },
		})
		// XSRF
		await axios.get(config.url + '/sanctum/csrf-cookie')
		const { data: createOrderData } = await axios.post(
			config.apiUrl + '/orders',
			order,
			{
				headers: {
					'X-XSRF-TOKEN': Cookies.get('XSRF-TOKEN')
				}
			}
		)

		if (!createOrderData || !createOrderData.data)
			throw new Error('Order not created')

		// Loaded
		dispatch({
			type: CREATE_ORDER_SUCCESS,
			payload: createOrderData && createOrderData.data,
		})

		localStorage.clear('cartItems')
		dispatch({ type: CLEAR_CART })
	} catch (e) {
		console.log(e)
		const error = `Something wrong has occurred. Details: ${e.message}`
		// Error
		dispatch({
			type: CREATE_ORDER_ERROR,
			payload: { error },
		})
	}
}

export const clearOrder = () => dispatch => {
	dispatch({ type: CLEAR_ORDER })
}

/**
 * Redux fetchOrders
 */
export const fetchOrders = () => async dispatch => {
	axios.defaults.withCredentials = true

	try {
		dispatch({
			type: GET_ORDERS_LOADING,
			payload: { loadingOrders: true },
		})
		// XSRF
		await axios.get(config.url + '/sanctum/csrf-cookie')
		const { data: ordersData } = await axios.get(
			config.apiUrl + '/orders',
			{
				headers: {
					'X-XSRF-TOKEN': Cookies.get('XSRF-TOKEN')
				}
			}
		)

		if (!ordersData || !ordersData.data)
			throw new Error('User not authenticated')
		// Loaded
		dispatch({
			type: GET_ORDERS_SUCCESS,
			payload: ordersData && ordersData.data,
		})
	} catch (e) {
		console.log(e)
		const error = `Something wrong has occurred. Details: ${e.message}`
		// Error
		dispatch({
			type: GET_ORDERS_ERROR,
			payload: { error },
		})
	}
}
