import axios from 'axios'
import Cookies from 'js-cookie'

import {
	// Login
	USER_LOGIN_LOADING,
	USER_LOGIN_ERROR,
	USER_LOGIN_SUCCESS,
	// Register
	USER_REGISTER_LOADING,
	USER_REGISTER_ERROR,
	USER_REGISTER_SUCCESS,
	// Current User
	GET_CURRENT_USER_ERROR,
	GET_CURRENT_USER_SUCCESS,
	USER_LOGOUT_ERROR,
	USER_LOGOUT_SUCCESS,
	// Clear orders
	CLEAR_ORDERS
} from '../types'
import config from '../config'

Window.Cookies = Cookies

/**
 * Redux loginAction
 *
 * @param {string} email
 * @param {string} password
 */
export const loginAction = (email, password) => async (dispatch) => {
	axios.defaults.withCredentials = true

	// Loading products
	dispatch({ type: USER_LOGIN_LOADING })

	try {
		// XSRF
		await axios.get(config.url + '/sanctum/csrf-cookie')

		const { data: loginData } = await axios.post(
			config.apiUrl + '/auth/login',
			{ email, password },
			{
				headers: {
					'X-XSRF-TOKEN': Cookies.get('XSRF-TOKEN')
				}
			}
		)

		if (!loginData || !loginData.data || !loginData.data.user)
			throw new Error('User not authenticated')
		// Loaded products
		dispatch({
			type: USER_LOGIN_SUCCESS,
			payload: { user: loginData && loginData.data && loginData.data.user },
		})
	} catch (e) {
		console.log(e)
		const error = `Something wrong has occurred. Details: ${e.message}`
		// Loaded products
		dispatch({
			type: USER_LOGIN_ERROR,
			payload: { error },
		})
	}
}

/**
 * Redux loginAction
 */
export const logoutAction = () => async (dispatch) => {
	axios.defaults.withCredentials = true

	try {
		// XSRF
		await axios.get(config.url + '/sanctum/csrf-cookie')
		const { data: logoutData } = await axios.get(
			config.apiUrl + '/auth/logout',
			{
				headers: {
					'X-XSRF-TOKEN': Cookies.get('XSRF-TOKEN')
				}
			}
		)

		if (!logoutData || !logoutData.data)
			throw new Error('User not logged out.')
		// dispatch success
		dispatch({ type: USER_LOGOUT_SUCCESS })
		dispatch({ type: CLEAR_ORDERS })
	} catch (e) {
		console.log(e)
		const error = `Something wrong has occurred. Details: ${e.message}`
		// dispatch error
		dispatch({
			type: USER_LOGOUT_ERROR,
			payload: { error },
		})
	}
}

/**
 * Redux registerAction
 *
 * @param {string} name
 * @param {string} email
 * @param {string} password
 */
export const registerAction = (name, email, password) => async (dispatch) => {
	axios.defaults.withCredentials = true

	// Loading products
	dispatch({ type: USER_REGISTER_LOADING })

	try {
		// XSRF
		await axios.get(config.url + '/sanctum/csrf-cookie')
		const { data: registerData } = await axios.post(
			config.apiUrl + '/auth/register',
			{ email, name, password },
			{
				headers: {
					'X-XSRF-TOKEN': Cookies.get('XSRF-TOKEN')
				}
			}
		)

		if (!registerData || !registerData.data || !registerData.data.user)
			throw new Error('User not registed')
		// Loaded products
		dispatch({
			type: USER_REGISTER_SUCCESS,
			payload: { user: registerData && registerData.data && registerData.data.user },
		})
	} catch (e) {
		console.log(e)
		const error = `Something wrong has occurred. Details: ${e.message}`
		// Loaded products
		dispatch({
			type: USER_REGISTER_ERROR,
			payload: { error },
		})
	}
}

/**
 * Redux getCurrentUserAction
 */
export const getCurrentUserAction = () => async (dispatch) => {
	axios.defaults.withCredentials = true

	try {
		// XSRF
		await axios.get(config.url + '/sanctum/csrf-cookie', {
			headers: {
				'X-Requested-With': 'XMLHttpRequest'
			}
		})
		const { data: currentUserData } = await axios.get(
			config.apiUrl + '/auth/current-user',
			{
				headers: {
					'X-XSRF-TOKEN': Cookies.get('XSRF-TOKEN')
				}
			}
		)

		if (!currentUserData || !currentUserData.data || !currentUserData.data.user)
			throw new Error('User not authenticated')
		// Loaded products
		dispatch({
			type: GET_CURRENT_USER_SUCCESS,
			payload: { user: currentUserData && currentUserData.data && currentUserData.data.user },
		})
	} catch (e) {
		const error = `Something wrong has occurred. Details: ${e.message}`
		// Loaded products
		dispatch({
			type: GET_CURRENT_USER_ERROR,
			payload: { error },
		})
	}
}
