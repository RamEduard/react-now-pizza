import React from 'react'
import {
	Avatar,
	Container,
	Grid,
	Link as MaterialLink,
	Typography,
	makeStyles,
} from '@material-ui/core'
import LockOutlinedIcon from '@material-ui/icons/LockOutlined'
import Alert from '@material-ui/lab/Alert'
import { connect, useSelector } from 'react-redux'
import { Link, useHistory } from 'react-router-dom'

// Actions
import { registerAction } from '../actions/userActions'

// Internal components
import RegisterForm from '../components/User/RegisterForm'

const useStyles = makeStyles(theme => ({
	paper: {
		marginTop: theme.spacing(8),
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
	},
}))

const RegisterContainer = ({ registerAction }) => {
	const classes = useStyles()
	const history = useHistory()
	const { currentUser, errorRegister, loadingRegister } = useSelector(state => state.user)

	const [registed, setRegistered] = React.useState()

	const onHandleSubmit = (values, actions) => {
		registerAction(values.name, values.email, values.password).then(() => {
			actions.setSubmitting(false)

			// Reset
			if (!errorRegister) {
				setRegistered(true)
				actions.resetForm()
			}
		})
	}

	React.useEffect(() => {
		if (currentUser) {
			setTimeout(() => {
				history.push('/')
			}, 1000)
		}
	}, [currentUser, history])

	React.useEffect(() => {
		if (registed && currentUser !== null) {
			setTimeout(() => {
				history.push('/')
			}, 1000)
		}
	}, [currentUser, history, registed])

	return (
		<React.Fragment>
			<Container maxWidth="xs">
				<div className={classes.paper}>
					<Avatar className={classes.avatar}>
						<LockOutlinedIcon />
					</Avatar>
					<Typography component="h1" variant="h5">
						Register
					</Typography>
					{errorRegister && <Alert severity="error">Error registering user.</Alert>}
					{!errorRegister && currentUser && registed && <Alert severity="success">Register successful. Redirecting...</Alert>}
					<RegisterForm loading={loadingRegister} onSubmit={onHandleSubmit} />
					<Grid container>
						<Grid item xs></Grid>
						<Grid item>
							<Link component={MaterialLink} to='/login'>
								{"Do you already have an account? Login"}
							</Link>
						</Grid>
					</Grid>
				</div>
			</Container>
		</React.Fragment>
	)
}

export default connect(null, { registerAction })(RegisterContainer)
