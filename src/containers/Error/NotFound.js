import React from 'react'
import { Container, Typography } from '@material-ui/core'

const NotFoundContainer = () => (
	<Container maxWidth="md">
		<Typography gutterBottom variant="h5" component="h1">
			404 | Not Found
		</Typography>
	</Container>
)

export default NotFoundContainer
