import React from 'react'
import {
	CircularProgress,
	Container,
	Grid,
	Snackbar,
	Typography,
} from '@material-ui/core'
import Alert from '@material-ui/lab/Alert'
import { connect } from 'react-redux'

import PizzaCard from '../components/PizzaCard'

// Actions
import { addToCart } from '../actions/cartActions'
import { getProductsAction } from '../actions/productActions'

// Internal components
import Banner from '../components/Banner'

// Internal hooks
import useProducts from '../hooks/useProducts'

import config from '../config'

const HomeContainer = ({ addToCart, getProductsAction }) => {
	const { error, loading, products } = useProducts()

	// States
	const [bannerInfo] = React.useState({
		description: "Now Pizza gives you the best experience...",
		bgImageSrc: config.url + '/img/pizza-banner-1_1920.jpg',
		logoImageSrc: config.url + '/img/nowpizza-logo.png',
		imgText: 'main image description'
	})
	const [openSnackbar, setOpenSnackbar] = React.useState()
	const [snackbarMessage, setSnackbarMessage] = React.useState('')
	const [snackBarSeverity, setSnackbarSeverity] = React.useState('info')

	React.useEffect(() => {
		const _currencySelected = localStorage.getItem('currencySelected')
		// Obtener productos
		getProductsAction(_currencySelected)
	}, [getProductsAction])

	// Event listeners
	const onOpenSnackbar = (message, type) => {
		if (['info', 'success', 'error', 'warning'].indexOf(type) >= 0) {
			setSnackbarSeverity(type)
		} else {
			setSnackbarSeverity('info')
		}

		setSnackbarMessage(message)
		setOpenSnackbar(true)
	}

	const onCloseSnackbar = (evt, reason) => {
		if (reason === 'clickaway') {
			return
		}

		setOpenSnackbar(false)
	}

	const onClickOrder = productData => {
		addToCart(productData)
		onOpenSnackbar('Added to shopping cart!', 'success')
	}

	return (
		<React.Fragment>
			<Banner data={bannerInfo} noButton />
			<Snackbar
				anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
				open={openSnackbar}
				autoHideDuration={2000}
				onClose={onCloseSnackbar}
			>
				<Alert severity={snackBarSeverity}>{`${snackbarMessage}`}</Alert>
			</Snackbar>
			<Container maxWidth="lg">
				{loading && (
					<Grid container justify="center">
						<CircularProgress />
					</Grid>
				)}
				<Grid container spacing={3}>
					<React.Fragment>
						{error && (
							<Typography variant="h5" paragraph>{`${error}`}</Typography>
						)}
						{!error && !loading && (!products || products.length <= 0) && (
							<Typography component="h3" variant="h4">There is not products data loaded.</Typography>
						)}
						{!error && !loading && products && products.length > 0 && products.map(product => (
							<PizzaCard
								key={product.id}
								currency={product.currency}
								description={product.description}
								image={product.image}
								images={product.images}
								price={product.price}
								sizes={product.sizes}
								title={product.title}
								onHandleOrder={({ count, price, size }) => onClickOrder({ ...product, count, price, size })}
							/>
						))}
					</React.Fragment>
				</Grid>
			</Container>
		</React.Fragment>
	)
}

export default connect(null,
	{
		addToCart,
		getProductsAction
	}
)(HomeContainer)
