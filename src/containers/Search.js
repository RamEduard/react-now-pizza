import React from 'react'
import { useParams } from 'react-router-dom'
import {
	CircularProgress,
	Container,
	Grid,
	Snackbar,
	Typography,
} from '@material-ui/core'
import Alert from '@material-ui/lab/Alert'
import { connect } from 'react-redux'

// Actions
import { addToCart } from '../actions/cartActions'
import { searchProducts } from '../actions/productActions'

// Internal hooks
import useCurrencySelected from '../hooks/useCurrencySelected'
import useSearchProducts from '../hooks/useSearchProducts'

// Internal components
import Banner from '../components/Banner'
import PizzaCard from '../components/PizzaCard'

import config from '../config'

const SearchContainer = ({ addToCart, searchProducts }) => {
	const { query } = useParams()
	const { error, products, loading } = useSearchProducts()
	const currencySelected = useCurrencySelected()

	// States
	const [bannerInfo, setBannerInfo] = React.useState()
	const [openSnackbar, setOpenSnackbar] = React.useState()
	const [snackbarMessage, setSnackbarMessage] = React.useState('')
	const [snackBarSeverity, setSnackbarSeverity] = React.useState('info')

	// Refs
	const isMountedRef = React.useRef()

	isMountedRef.current = true

	React.useEffect(() => {
		if (isMountedRef.current) {
			setBannerInfo({
				title: "Search your favorite pizza",
				bgImageSrc: config.url + '/img/pizza-banner-1_1920.jpg',
				logoImageSrc: config.url + '/img/nowpizza-logo.png',
				imgText: 'main image description'
			})
		}
		return () => (isMountedRef.current = false)
	}, [])

	React.useEffect(() => {
		if (query !== '' && searchProducts) {
			// Obtener productos
			searchProducts(query, currencySelected)
		}
	}, [query, currencySelected, searchProducts])

	// Event listeners
	const onOpenSnackbar = (message, type) => {
		if (['info', 'success', 'error', 'warning'].indexOf(type) >= 0) {
			setSnackbarSeverity(type)
		} else {
			setSnackbarSeverity('info')
		}

		setSnackbarMessage(message)
		setOpenSnackbar(true)
	}

	const onCloseSnackbar = (evt, reason) => {
		if (reason === 'clickaway') {
			return
		}

		setOpenSnackbar(false)
	}

	const onClickOrder = productData => {
		addToCart(productData)
		onOpenSnackbar('Added to shopping cart!', 'success')
	}

	return (
		<React.Fragment>
			<Snackbar
				anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
				open={openSnackbar}
				autoHideDuration={2000}
				onClose={onCloseSnackbar}
			>
				<Alert severity={snackBarSeverity}>{`${snackbarMessage}`}</Alert>
			</Snackbar>
			{/* Banner */}
			<Banner data={bannerInfo} noLogo noButton />
			<Container maxWidth="lg">
				<Grid container spacing={3}>
					{loading && (
						<Grid container justify="center">
							<CircularProgress />
						</Grid>
					)}
					<React.Fragment>
						{!error && !loading && (!products || products.length <= 0) && (
							<Typography variant="h5" paragraph>There is not products with title or description: <strong>{`${query}`}</strong>.</Typography>
						)}
						{error && (
							<Typography variant="h5" paragraph>{`${error}`}</Typography>
						)}
						{!error && products && products.length > 0 && products.map(product => (
							<PizzaCard
								key={product.id}
								currencySelected={currencySelected}
								description={product.description}
								image={product.image}
								price={product.price}
								sizes={product.sizes}
								title={product.title}
								onHandleOrder={({ count, size }) => onClickOrder({ ...product, count, size })}
							/>
						))}
					</React.Fragment>
				</Grid>
			</Container>
		</React.Fragment>
	)
}

export default connect(null, { addToCart, searchProducts })(SearchContainer)
