import React from 'react'
import {
	Avatar,
	Container,
	Grid,
	Link as MaterialLink,
	Typography,
	makeStyles,
} from '@material-ui/core'
import LockOutlinedIcon from '@material-ui/icons/LockOutlined'
import Alert from '@material-ui/lab/Alert'
import { connect, useSelector } from 'react-redux'
import { Link, useHistory } from 'react-router-dom'

// Actions
import { loginAction } from '../actions/userActions'

// Internal components
import LoginForm from '../components/User/LoginForm'

const useStyles = makeStyles(theme => ({
	paper: {
		marginTop: theme.spacing(8),
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
	},
}))

const LoginContainer = ({ loginAction }) => {
	const classes = useStyles()
	const history = useHistory()
	const { currentUser, errorLogin, loadingLogin } = useSelector(state => state.user)

	const [loggedIn, setLoggedIn] = React.useState()

	const onHandleSubmit = (values, actions) => {
		loginAction(values.email, values.password).then(() => {
			actions.setSubmitting(false)

			// Reset
			if (!errorLogin) {
				setLoggedIn(true)
				actions.resetForm()
			}
		})
	}

	React.useEffect(() => {
		if (currentUser) {
			setLoggedIn(true)
		}
	}, [currentUser])

	React.useEffect(() => {
		if (loggedIn && currentUser !== null) {
			setTimeout(() => {
				history.push('/')
			}, 1000)
		}
	}, [currentUser, history, loggedIn])

	return (
		<React.Fragment>
			<Container maxWidth="xs">
				<div className={classes.paper}>
					<Avatar className={classes.avatar}>
						<LockOutlinedIcon />
					</Avatar>
					<Typography component="h1" variant="h5">
						Sign in
					</Typography>
					{errorLogin && <Alert severity="error">Bad credentials.</Alert>}
					{!errorLogin && currentUser && loggedIn && <Alert severity="success">Login successful. Redirecting...</Alert>}
					<LoginForm loading={loadingLogin} onSubmit={onHandleSubmit} />
					<Grid container>
						<Grid item xs></Grid>
						<Grid item>
							<Link to="/register" component={MaterialLink}>
								{"Don't have an account? Register"}
							</Link>
						</Grid>
					</Grid>
				</div>
			</Container>
		</React.Fragment>
	)
}

export default connect(null, { loginAction })(LoginContainer)
