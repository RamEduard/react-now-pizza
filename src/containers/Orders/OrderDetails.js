import React from 'react'
import {
	Container,
	Grid,
	Paper,
	Typography,
	makeStyles,
	List,
	ListItem,
	ListItemText,
} from '@material-ui/core'
import moment from 'moment'
import { useParams } from 'react-router-dom'
import { connect } from 'react-redux'

// Internal actions
import { fetchOrders } from '../../actions/orderActions'

// Internal hooks
import useOrders from '../../hooks/useOrders'

// Internal components
import Breadcrumb from '../../components/Breadcrumb'
import OrderProductsTable from '../../components/Table/OrderProductsTable'
import CartTotals from '../../components/Cart/CartTotals'

const useStyles = makeStyles(theme => ({
	paper: {
		marginTop: theme.spacing(2),
		marginBottom: theme.spacing(2),
		padding: theme.spacing(2),
	},
}))

const ContactData = ({ className, data }) => (
	<Paper className={className}>
		<Typography component="h3" variant="h5">
			Contact Data
		</Typography>
		<List dense>
			<ListItem disableGutters>
				<Grid container>
					<Grid item md={6} xs={12}>
						{/* Email */}
						<ListItemText primary="Email" secondary={data.email} />
					</Grid>
					<Grid item md={6} xs={12}>
						{/* Phone */}
						<ListItemText primary="Phone number" secondary={data.phone} />
					</Grid>
				</Grid>
			</ListItem>
		</List>
	</Paper>
)

const AddressInfo = ({ className, data }) => (
	<Paper className={className}>
		<Typography component="h3" variant="h5">
			{{
				both: 'Billing & Shipping',
				billing: 'Billing',
				shipping: 'Shipping'
			}[data.type]} Address
		</Typography>
		<List dense>
			<ListItem disableGutters>
				<Grid container>
					<Grid item md={6} xs={12}>
						{/* First Name */}
						<ListItemText primary="First name" secondary={data.first_name} />
					</Grid>
					<Grid item md={6} xs={12}>
						{/* Last Name */}
						<ListItemText primary="Last name" secondary={data.last_name} />
					</Grid>
				</Grid>
			</ListItem>
			<ListItem disableGutters>
				<Grid container>
					<Grid item md={6} xs={12}>
						{/* Address1 */}
						<ListItemText primary="Address line 1" secondary={data.address1} />
					</Grid>
					<Grid item md={6} xs={12}>
						{/* Address2 */}
						<ListItemText primary="Address line 2" secondary={data.address2} />
					</Grid>
				</Grid>
			</ListItem>
			<ListItem disableGutters>
				<Grid container>
					<Grid item md={6} xs={12}>
						{/* Country */}
						<ListItemText primary="Country" secondary={data.country} />
					</Grid>
					<Grid item md={6} xs={12}>
						{/* Country */}
						<ListItemText primary="City" secondary={data.city} />
					</Grid>
				</Grid>
			</ListItem>
			<ListItem disableGutters>
				<Grid container>
					<Grid item md={6} xs={12}>
						{/* Street */}
						<ListItemText primary="State/Province/Region" secondary={data.state} />
					</Grid>
					<Grid item md={6} xs={12}>
						{/* Zip */}
						<ListItemText primary="Postal code / Zip" secondary={data.zip} />
					</Grid>
				</Grid>
			</ListItem>
		</List>
	</Paper>
)

const breadcrumbLinks = [
	{
		path: '/',
		label: 'Home'
	},
	{
		path: '/orders',
		label: 'Orders'
	},
	{
		label: 'Order details'
	}
]

/**
 * OrderDetailsContainer
 */
const OrderDetailsContainer = ({ fetchOrders }) => {
	const classes = useStyles()
	const { id } = useParams()
	const ordersData = useOrders()

	const [orderDetails, setOrderDetails] = React.useState(null)

	let isMounted = true
	React.useEffect(() => {
		if (isMounted) {
			fetchOrders()
		}
		return () => isMounted = false
	}, [])

	React.useEffect(() => {
		if (isMounted && id && id !== '' && ordersData && ordersData.length > 0 && orderDetails === null) {
			const orderFound = ordersData.filter(o => o.id === id)
			setOrderDetails(orderFound[0])
		}
	}, [id, isMounted, ordersData, orderDetails, setOrderDetails])

	return (
		<Container maxWidth="lg">
			<Typography component="h1" variant="h4" align="center">
				Order details
			</Typography>
			{/* Breadcrumbs */}
			<Breadcrumb links={breadcrumbLinks} />
			<br />
			{!orderDetails && (
				<Typography component="h3" variant="h3" align="center">
					Order not found.
				</Typography>
			)}
			{orderDetails && (
				<React.Fragment>
					<Typography variant="body1" style={{marginBottom: 16}}>
						Order #{orderDetails.id} was placed on {moment(orderDetails.created_at).format('DD MMM YYYY')} and it has a status {orderDetails.status}.
					</Typography>
					<Grid container spacing={2}>
						{/* Products */}
						<Grid item lg={9} md={8} xs={12}>
							<OrderProductsTable currency={orderDetails.currency} products={orderDetails.products} />
						</Grid>
						<Grid item lg={3} md={4} xs={12}>
							<CartTotals
								currency={orderDetails.currency}
								shipping={orderDetails.shipping}
								subtotal={orderDetails.subtotal}
								total={orderDetails.total}
							/>
						</Grid>
						{/* AddressDatas */}
						{orderDetails.addressDatas && orderDetails.addressDatas.length > 0 && orderDetails.addressDatas.map((addressData, index) => (
							<Grid item key={`adressData${index}`} md={6} xs={12}>
								<AddressInfo className={classes.paper} data={orderDetails.addressDatas[0]} />
							</Grid>
						))}
						<Grid item md={6} xs={12}>
							<ContactData className={classes.paper} data={orderDetails.contactData} />
						</Grid>
					</Grid>
				</React.Fragment>
			)}
		</Container>
	)
}

export default connect(null, { fetchOrders })(OrderDetailsContainer)
