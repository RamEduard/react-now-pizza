import React from 'react'
import { Container, Paper, Typography, makeStyles } from '@material-ui/core'
import { connect } from 'react-redux'

// Internal actions
import { fetchOrders } from '../../actions/orderActions'

// Internal hooks
import useOrders from '../../hooks/useOrders'

// Internal components
import Breadcrumb from '../../components/Breadcrumb'
import OrdersTable from '../../components/Table/OrdersTable'

const useStyles = makeStyles(theme => ({
	paper: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
		paddingTop: theme.spacing(2),
	}
}))

const breadcrumbLinks = [
	{
		path: '/',
		label: 'Home'
	},
	{
		label: 'Order'
	}
]

/**
 * OrdersContainer
 */
const OrdersContainer = ({ fetchOrders }) => {
	const classes = useStyles()
	const ordersData = useOrders()

	let isMounted = true
	React.useEffect(() => {
		if (isMounted) {
			fetchOrders()
		}
		return () => isMounted = false
	}, [])

	return (
		<Container maxWidth="lg">
			<Typography component="h1" variant="h4" align="center">
				Orders
			</Typography>
			{/* Breadcrumbs */}
			<Breadcrumb links={breadcrumbLinks} />
			<Paper className={classes.paper}>
				<OrdersTable data={ordersData} />
			</Paper>
		</Container>
	)
}

export default connect(null, { fetchOrders })(OrdersContainer)
