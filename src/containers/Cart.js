import React from 'react'
import {
	Container,
	Grid,
	Typography,
	makeStyles,
	Button,
} from '@material-ui/core'
import { connect } from 'react-redux'
import { useHistory } from 'react-router-dom'

// Internal actions
import { countProduct, removeFromCart } from '../actions/cartActions'

// Internal hooks
import useCurrencySelected from '../hooks/useCurrencySelected'
import useCartItems from '../hooks/useCartItems'
import useCartTotals from '../hooks/useCartTotals'

// Internal components
import Breadcrumb from '../components/Breadcrumb'
import CartTable from '../components/Table/CartTable'
import CartTotals from '../components/Cart/CartTotals'


const useStyles = makeStyles(theme => ({
	actionButton: {
		marginTop: theme.spacing(2),
		textAlign: 'right',
	},
	margin: {
		marginTop: theme.spacing(3),
		marginBottom: theme.spacing(3),
		paddingTop: theme.spacing(2),
	},
}))

const breadcrumbLinks = [
	{
		path: '/',
		label: 'Home',
	},
	{
		label: 'Shopping cart',
	},
]

const CartContainer = ({ removeFromCart, countProduct }) => {
	const classes = useStyles()
	const history = useHistory()
	const currencySelected = useCurrencySelected()
	const cartItems = useCartItems()
	const cartTotals = useCartTotals()

	// Events
	const handleOnRemoveFromCart = item => {
		removeFromCart(item)
	}

	return (
		<Container maxWidth="lg">
			<Typography component="h1" variant="h4" align="center">
				Shopping Cart
			</Typography>
			{/* Breadcrumbs */}
			<Breadcrumb links={breadcrumbLinks} />
			<Grid container spacing={2} className={classes.margin}>
				<Grid item lg={9} md={8} xs={12}>
					<CartTable
						currency={currencySelected}
						items={cartItems}
						onRemoveFromCart={handleOnRemoveFromCart}
						onCountProduct={countProduct}
					/>
				</Grid>
				{cartItems && cartItems.length > 0 && (
					<Grid item lg={3} md={4} xs={12}>
						<CartTotals
							currency={cartItems[0].currency}
							shipping={null}
							subtotal={cartTotals.subtotal}
							total={cartTotals.total}
						/>
						<div className={classes.actionButton}>
							<Button
								variant="outlined"
								color="primary"
								onClick={() => history.push('/checkout')}
								fullWidth
							>
								Proceed to checkout
							</Button>
						</div>
					</Grid>
				)}
			</Grid>
		</Container>
	)
}

export default connect(null, {
	countProduct,
	removeFromCart,
})(CartContainer)
