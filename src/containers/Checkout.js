import React from 'react'
import {
	Button,
	Container,
	Grid,
	makeStyles,
	Paper,
	Typography,
	Stepper,
	Step,
	StepLabel,
} from '@material-ui/core'
import { connect } from 'react-redux'
import { useHistory } from 'react-router-dom'

// Internal actions
import { addShippingCostAction } from '../actions/cartActions'
import { createOrder } from '../actions/orderActions'

// Internal components
import Breadcrumb from '../components/Breadcrumb'
import CartTotals from '../components/Cart/CartTotals'
import AddressForm from '../components/Checkout/AddressForm'
import ContactDataForm from '../components/Checkout/ContactDataForm'
import Review from '../components/Checkout/Review'

// Internal hooks
import useCartTotals from '../hooks/useCartTotals'
import useCartItems from '../hooks/useCartItems'
import useCurrencySelected from '../hooks/useCurrencySelected'

const useStyles = makeStyles(theme => ({
	paper: {
		padding: theme.spacing(2),
	},
	stepper: {
		padding: theme.spacing(3, 0, 5),
	},
	buttons: {
		display: 'flex',
		justifyContent: 'flex-end',
	},
	button: {
		marginTop: theme.spacing(3),
		marginLeft: theme.spacing(1),
	},
	margin: {
		marginTop: theme.spacing(2),
		marginBottom: theme.spacing(2),
	},
}))

const STEPS = [
	'Shipping address',
	'Contact info',
	'Review your order',
]

const breadcrumbLinks = [
	{
		path: '/',
		label: 'Home',
	},
	{
		path: '/cart',
		label: 'Cart',
	},
	{
		label: 'Checkout',
	},
]

/**
 * CheckoutContainer
 */
const CheckoutContainer = ({ addShippingCostAction, createOrder }) => {
	const classes = useStyles()
	const cartItems = useCartItems()
	const cartTotals = useCartTotals()
	const currencySelected = useCurrencySelected()
	const history = useHistory()

	const [activeStep, setActiveStep] = React.useState(0)
	const [contactData, setContactData] = React.useState()
	const [shippingData, setShippingData] = React.useState()

	React.useEffect(() => {
		if (!cartItems.length && activeStep === 0) {
			history.push('/')
		}
	}, [cartItems, history, activeStep])

	// Event listeners
	const handleBack = () => {
		setActiveStep(activeStep - 1)
	}

	const onSubmitAddressForm = values => {
		setShippingData(values)
		if (values.zip !== '') {
			addShippingCostAction(3)
		}
	}

	const onSubmitContactDataForm = values => {
		setContactData(values)
	}

	const handleNext = () => {
		// Place Order
		if (activeStep === 2) {
			// Create order
			createOrder({
				currency: currencySelected,
				subtotal: cartTotals.subtotal,
				shipping: cartTotals.shipping,
				total: cartTotals.total,
				status: 'pending',
				addressDatas: [shippingData],
				contactData: contactData,
				products: cartItems,
			})
			// window.location.replace('/orders')
		}

		setActiveStep(activeStep + 1)
	}

	return (
		<Container maxWidth="lg">
			<Typography component="h1" variant="h4" align="center">
				Checkout
			</Typography>
			{/* Breadcrumbs */}
			<Breadcrumb links={breadcrumbLinks} />
			<Grid container spacing={2} className={classes.margin}>
				<Grid item lg={9} md={8} xs={12}>
					<Paper className={classes.paper}>
						<Stepper activeStep={activeStep} className={classes.stepper}>
							{STEPS.map(step => (
								<Step key={step}>
									<StepLabel>{step}</StepLabel>
								</Step>
							))}
						</Stepper>
						<React.Fragment>
							{activeStep === STEPS.length ? (
								<React.Fragment>
									<Typography variant="h5" gutterBottom>
										Thank you for your order.
									</Typography>
									<Typography variant="subtitle1">
										We have received your order, and will send you
										an update when your order has delivered.
									</Typography>
								</React.Fragment>
							) : (
								<React.Fragment>
									{activeStep === 0 && <AddressForm initialValues={shippingData} onSubmit={onSubmitAddressForm} />}
									{activeStep === 1 && <ContactDataForm initialValues={contactData} onSubmit={onSubmitContactDataForm} />}
									{activeStep === 2 && (
										<Review cartItems={cartItems} contactData={contactData} shippingData={shippingData} />
									)}
									<div className={classes.buttons}>
										{activeStep !== 0 && (
											<Button onClick={handleBack} className={classes.button}>
												Back
											</Button>
										)}
										<Button
											variant="contained"
											color="primary"
											onClick={handleNext}
											disabled={
												((!shippingData ||
												shippingData === null ||
												shippingData === {}) && activeStep === 0) ||
												((!contactData ||
													contactData === null ||
													contactData === {}) && activeStep === 1)
											}
											className={classes.button}
										>
											{activeStep === STEPS.length - 1 ? 'Place order' : 'Next'}
										</Button>
									</div>
								</React.Fragment>
							)}
						</React.Fragment>
					</Paper>
				</Grid>
				{activeStep !== STEPS.length && (
					<Grid item lg={3} md={4} xs={12}>
						<CartTotals
							currency={cartItems && cartItems.length > 0 && cartItems[0].currency}
							shipping={cartTotals.shipping === 0 ? null : cartTotals.shipping}
							subtotal={cartTotals.subtotal}
							total={cartTotals.total}
						/>
					</Grid>
				)}
			</Grid>
		</Container>
	)
}

export default connect(null, { addShippingCostAction, createOrder })(CheckoutContainer)
