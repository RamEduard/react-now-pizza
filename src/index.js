import * as serviceWorker from './serviceWorker'
import './index.css'
import './components/Root'

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
if (process.env.NODE_ENV === 'development') {
	serviceWorker.unregister()
} else {
	serviceWorker.register()
}
