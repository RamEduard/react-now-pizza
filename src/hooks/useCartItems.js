import { useSelector } from 'react-redux'

const useCartItems = () => useSelector(state => state.cart && state.cart.cartItems)

export default useCartItems
