import { useSelector } from 'react-redux'

const useCurrencySelected = () => useSelector(state => state.currency && state.currency.currencySelected)

export default useCurrencySelected
