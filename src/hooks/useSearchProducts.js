import { useSelector } from 'react-redux'

const useSearchProducts = () => {
	const { error, filteredItems, loading } = useSelector(state => state.products)

	const products = filteredItems && filteredItems.data && filteredItems.data.map(o => {
		return {id: o.id, ...o.attributes}
	})

	return { error, products, loading }
}

export default useSearchProducts
