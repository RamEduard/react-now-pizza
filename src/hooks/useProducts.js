import { useSelector } from 'react-redux'

const useProducts = () => {
	const { errorHome, filteredItems, loadingHome } = useSelector(state => state.products)

	const products = filteredItems && filteredItems.data && filteredItems.data.map(p => {
		return {id: p.id, ...p.attributes}
	})

	return {error: errorHome, loading: loadingHome, products }
}

export default useProducts
