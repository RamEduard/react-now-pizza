import { useSelector } from 'react-redux'

const useExchangeRates = () =>{
	const exchangeRates = useSelector(state => state.currency && state.currency.exchangeRates)

	if (exchangeRates && exchangeRates.data)
		return { id: exchangeRates.data.id, ...exchangeRates.data.attributes }
}

export default useExchangeRates
