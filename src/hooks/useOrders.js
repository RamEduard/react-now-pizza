import { useSelector } from 'react-redux'

const useOrders = () => {
	const ordersData = useSelector(state => state.order && state.order.orders)

	return ordersData && ordersData.map(o => {
		return {id: o.id, ...o.attributes}
	})
}

export default useOrders
