import React from 'react'
import { useSelector } from 'react-redux'

const useCartTotals = () => {
	const cartItems = useSelector(state => state.cart && state.cart.cartItems)
	const shippingCost = useSelector(state => state.cart && state.cart.shippingCost)

	const [cartTotals, setCartTotals] = React.useState({
		shipping: 0,
		subtotal: 0,
		total: 0,
	})

	React.useEffect(() => {
		if ((cartItems && cartItems.length) || shippingCost > 0) {
			// Calculate totals
			const shipping = shippingCost
			let subtotal = 0

			cartItems.forEach(item => {
				subtotal += item.count * item.price
			})

			setCartTotals({
				shipping,
				subtotal,
				total: shipping + subtotal,
			})
		}
	}, [cartItems, shippingCost])

	return cartTotals
}

export default useCartTotals
