import React from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'

import LayoutComponent from './components/Layout'

// Containers
import CartContainer from './containers/Cart'
import CheckoutContainer from './containers/Checkout'
import LoginContainer from './containers/Login'
import HomeContainer from './containers/Home'
import NotFoundContainer from './containers/Error/NotFound'
import OrdersContainer from './containers/Orders/Orders'
import OrderDetailsContainer from './containers/Orders/OrderDetails'
import RegisterContainer from './containers/Register'
import SearchContainer from './containers/Search'

const ROUTES = [
	{
		path: '/',
		Component: HomeContainer,
		useLayout: true
	},
	{
		path: '/cart',
		Component: CartContainer,
		useLayout: true
	},
	{
		path: '/checkout',
		Component: CheckoutContainer,
		useLayout: true
	},
	{
		path: '/orders',
		Component: OrdersContainer,
		useLayout: true
	},
	{
		path: '/order/:id',
		Component: OrderDetailsContainer,
		useLayout: true
	},
	{
		path: '/search/:query',
		Component: SearchContainer,
		useLayout: true
	},
	{
		path: '/login',
		Component: LoginContainer,
		useLayout: true
	},
	{
		path: '/register',
		Component: RegisterContainer,
		useLayout: true
	},
	{
		path: '*',
		Component: NotFoundContainer,
		useLayout: true
	},
]

const RoutesComponent = () => (
	<BrowserRouter>
		<Switch>
			{ROUTES.map(({ Component, path, Props, useLayout }) => {
				if (useLayout) {
					return <Route
						exact
						key={path}
						path={path}
						render={props => (
							<LayoutComponent>
								<Component {...props} {...Props} />
							</LayoutComponent>
						)}
					/>
				}
				return <Route exact key={path} path={path} render={props => <Component {...props} {...Props} />} />
			})}
		</Switch>
	</BrowserRouter>
)

export default RoutesComponent
