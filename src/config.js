const PRODUCTION = {
	apiUrl: 'http://api.nowpizza.xyz/api/v1',
	url: 'http://api.nowpizza.xyz'
}

const TEST = {
	apiUrl: 'http://api.nowpizza.xyz/api/v1',
	url: 'http://api.nowpizza.xyz'
}

const DEVELOPMENT = {
	apiUrl: 'http://localhost:8000/api/v1',
	url: 'http://localhost:8000'
}

export default process.env.NODE_ENV === "production"
  ? PRODUCTION
  : process.env.NODE_ENV === "test"
  ? TEST
  : DEVELOPMENT
